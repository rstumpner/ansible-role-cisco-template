## create virtual environment
python -m venv .venv
source .venv/bin/activate

## install pythong packages
pip install requirements.txt
- docker
- molecule
- molecule-docker
- ansible
 
## molecule 
molecule test