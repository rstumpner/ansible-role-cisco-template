import pytest
from ciscoconfparse import CiscoConfParse

# vars for config path
cisco_config_file_path = ['../../tests/build/ansible-role-cisco-template/sandbox-iosxe-latest-1.cisco.com-ansible-role-cisco-template.cli']
#
hostname_min = 1
hostname_max = 63

@pytest.mark.parametrize('cisco_config_cli', cisco_config_file_path )
#@pytest.mark.parametrize('cisco_config_cli', ['../ciscoconfparse/example-cli.conf'])
def test_cisco_config_cli_hostname(cisco_config_cli):
    ciscoconfigcli = CiscoConfParse(cisco_config_cli, syntax='ios')
    for cli_obj in ciscoconfigcli.find_objects('^hostname'):
         assert cli_obj is not 0 , "no Hostname is set"
         assert len(cli_obj.text)  >= hostname_min , "Hostname is to short"
         assert len(cli_obj.text)  <= hostname_max , "Hostname is to long"