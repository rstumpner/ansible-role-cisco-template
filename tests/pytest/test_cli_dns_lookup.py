import pytest
from ciscoconfparse import CiscoConfParse
from netaddr import *

# vars for config path
cisco_config_file_path = ['../../tests/build/ansible-role-cisco-template/sandbox-iosxe-latest-1.cisco.com-ansible-role-cisco-template.cli']
# vars for tests
hostname_min = 1
hostname_max = 63

@pytest.mark.parametrize('cisco_config_cli', cisco_config_file_path )
#@pytest.mark.parametrize('cisco_config_cli', ['../ciscoconfparse/example-cli.conf'])
def test_cisco_config_cli_dns_lookup(cisco_config_cli):
    ciscoconfigcli = CiscoConfParse(cisco_config_cli, syntax='ios')
    for cli_obj in ciscoconfigcli.find_objects(r"^ip domain lookup"):
         assert cli_obj is not 0 , "Domain Lookup is not set"

@pytest.mark.parametrize('cisco_config_cli', cisco_config_file_path)
#@pytest.mark.parametrize('cisco_config_cli', ['../ciscoconfparse/example-cli.conf'])
def test_cisco_config_cli_ip_nameserver(cisco_config_cli):
    ciscoconfigcli = CiscoConfParse(cisco_config_cli, syntax='ios')
    for cli_obj in ciscoconfigcli.find_objects(r"^ip name-server"):
         assert cli_obj is not 0 , "Nameserver is not set"
#         assert IPAddress(cli_obj(1)).version() is 6 , "Nameserver is not IPv4"
