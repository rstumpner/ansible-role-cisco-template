import pytest
from ciscoconfparse import CiscoConfParse

# vars for config path
cisco_config_file_path = ['../../tests/build/ansible-role-cisco-template/sandbox-iosxe-latest-1.cisco.com-ansible-role-cisco-template.cli']


vlan_min = 1
vlan_max = 20

@pytest.mark.parametrize('cisco_config_cli', cisco_config_file_path )

def test_cisco_config_cli_vlan(cisco_config_cli):
    ciscoconfigcli = CiscoConfParse(cisco_config_cli, syntax='ios')
    for cli_obj in ciscoconfigcli.find_objects_w_child(r'^vlan',r'^name'):
         assert cli_obj is not 0 , "no vlan name is set"
         assert len(cli_obj.text)  >= vlan_min , "vlan name is to short"
         assert len(cli_obj.text)  <= vlan_max , "vlan name is to long"