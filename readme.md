# Ansible Role Template for Cisco Configuration

This is a Ansible Template Role for generating Cisco Configuration and Tests itself with a CI/CD Pipeline that is contained in the Repository.So it can be used as a starting point for a new ansible role or to test some Jinja2 templates.

## Requirements:
   * Ansible 2.4+
   * python 3
   - ansible collection cisco.ios.collection

## Installation:

For the first steps there is no Installation needed just clone it to your Gitlab ( https://gitlab.com ) repository and the Gitlab Runner should work.
If you want to do some test on your local machine install a Gitlab Runner there or Install Ansible (pip install ansible) and Copy & Paste the CI/CD Stages from the gitlab-ci.yml file.


## Using this Role:
Drive to the Ansible Role Directory:
    * git clone https://gitlab.com/rstumpner/ansible-role-cisco-template.git

Activate this Role in a Playbook:

Example:
```YAML
- hosts: all
  geather_facts: no
  roles:
     - ansible-role-cisco-template
```
For some example configurations have a look at the main.yml file in the defaults folder (https://gitlab.com/rstumpner/ansible-role-cisco-template/-/blob/master/defaults/main.yml) in this repository.

## Features:
- Hostname Configuration
- Interface L2 and L3 Configuration
- VRF Configuration

## CI/CD Stages
A short overview of the implemented Stages in the CI/CD Pipeline.

#### Validate

There should be the tasks to validate the imput sources YAML files for example. Actually its a simple Ansible syntaxcheck.

#### Build
All Tasks for building the configurations (cli/netconf/restconf) will be executed and saved as artifact.

Ansible tags: 
- build
- build-cli
- build-netconf
- build-restconf

#### Test
All Tasks for Testing the configuration or the devices are placed here . Also some pre deployment tasks could be done here like a simple configuration snapshot of the device.

Ansible Tags:
- bak
- diff
- tests

#### deploy
The deployment of the configuration will be in this stage. 

Ansible Tags:
- deploy-cli
- deploy-oam
- deploy 

## Tested:
 - Cisco IOS
 - Cisco IOS-XE

## feature hostname
Set a hostname on the cisco device

### stage static tests (sast)
default: on 
tags:
   - sast
   - static_test
feature flags:
    - cisco_template_sast|default(true) == true
    - cisco_template_sast_hostname|default(false) == true
tests:
    - is defined
    - not more than 64 Chars


### stage build
default: on 
tags:
   - build
   - build_cli
feature flags:
    - cisco_template_build|default(true) == true
    - cisco_template_build_cli|default(true) == true
    - cisco_template_build_cli_ios|default(true) == true 

## feature backup
Snapshot off the device config:
- running
- state (facts/json)
- write running config to startup
- generate diff from run to desired state


### stage prepare
default: on 
tags:
    - prepare
feature flags:
    - cisco_template_prepare|default(true) == true
    - cisco_template_prepare_backup|default(true) == true
    - cisco_template_prepare_facts|default(true) == true
    - cisco_template_prepare_diff|default(true) == true

### stage deploy oam

default: on
tags:
    - deploy
    - oam
    - cli
    - deploy-oam
feature flags:
    - not ansible_check_mode
    - cisco_template_deploy|default(false) == true
    - cisco_template_deploy_cli|default(true) == true
    - cisco_template_deploy_cli_ios|default(false) == true

### stage deploy oam

default: on
tags:
    - deploy
    - oam
    - cli
    - deploy-oam
feature flags:
    - not ansible_check_mode
    - cisco_template_deploy|default(false) == true
    - cisco_template_deploy_cli|default(true) == true
    - cisco_template_deploy_cli_ios|default(false) == true

### Stage verify (dast)
Tests:
- hostname from facts

default: on
tags:
    - deploy
    - oam
    - cli
    - deploy-oam
feature flags:
    - not ansible_check_mode
    - cisco_template_verify|default(false) == true
    - cisco_template_verify_hostname|default(false) == true
#    - cisco_template_verify_interfaces|default(false) == true
#    - cisco_template_verify_icmp|default(false) == true

## feature inventory pyats
Build an Inventory for the Cisco pyats toolbox

### stage build
default: on 
tags:
   - build
   - inventory
feature flags:
    - cisco_template_build_inv_pyats|default(false) == true

### stage prepare
default: on 
tags:
   - prepare
feature flags:
    - cisco_template_prepare|default(true) == true
    - cisco_template_prepare_pyats|default(false) == true

## Known Issues
Just have a look at this Repository with the tag fixes.

https://gitlab.com/rstumpner/ansible-role-cisco-template/-/issues

## How to Contribute
Just open up an issue or get in contact via email. Actually it is under havy development and there are many ideas to contribute to this project. 

## License:
    MIT / BSD

## Author Information:
roland@stumpner.at