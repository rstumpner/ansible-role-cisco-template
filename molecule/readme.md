## Testing with moleclue
### Install on OS
apt-get install python3 python3-setuptools

### create a virtal environment (venv)
python3 -m venv .venv
## enter venv
source .venv/bin/activate

## Install Molecule on venv
pip install requirements.txt

## testing
molecule test -s default

