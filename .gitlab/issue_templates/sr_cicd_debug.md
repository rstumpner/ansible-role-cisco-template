## Settings for CI/CD to debug Ansible Tasks
- normal setting is to write the ansible output to an junit test file and upload this to gitlab reports for using gitlabs report ui on merge requests
- sometimes especilay in havy development times on roles it is not easy to get the output from the junit test file with 2 steps you get the output from ansible to the CI/CD detail UI

- edit gitlab-ci.yml
```
# just commt the following lines

#  ANSIBLE_LOG_PATH: 'tests/state/ansible-play.log'
#  ANSIBLE_STDOUT_CALLBACK: 'junit'
```